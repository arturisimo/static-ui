image: node # global image

stages: # List of stages for jobs, and their order of execution
    - build
    - test
    - deploy_review
    - deploy_staging
    - deploy_prod
    - post_deploy
    - cache

# caching node_modules using as key the name of the branch (CI_COMMIT_REF_NAME) in lowercase and shortened with no special char
# specifying the pull policy: the jobs just download the cache when the job starts, but never upload changes when the job finishes.
cache:
  key: ${"$CI_COMMIT_REF_SLUG"} 
  paths:
    - node_modules/
  policy: pull

variables:
  STAGING_DOMAIN: third-jam-staging.surge.sh
  PROD_DOMAIN: third-jam.surge.sh

# since cache is never updating and stage to update node_modules is set scheduled
update_cache:
  stage: cache
  script:
    - npm install
  cache:
      key: ${CI_COMMIT_REF_SLUG}
      paths:
        - node_modules/
      policy: push
  only:
    - schedules

build:
  stage: build
  only: 
    - main
    - merge_request
  script:
    - npm install
    - npm install -g gatsby-cli
    - gatsby build
    - sed -i "s/%%VERSION%%/$CI_COMMIT_SHORT_SHA/" ./public/index.html # replace tag with last commit
  artifacts:
    paths:
      - ./public
  except:
   - schedules      

test_artifact:
  image: alpine
  stage: test
  only:
    - main
    - merge_request
  script:
    - grep "Gatsby" ./public/index.html
  except:
   - schedules    
test_website:
  stage: test
  only:
    - main
    - merge_request
  script:
    - npm install
    - npm install -g gatsby-cli
    - gatsby serve & # & > process in background
    - sleep 3
    - curl "http://localhost:9000" | grep -q "Gatsby"
  except:
   - schedules   

# Deploying in review: dynamic environment previous to the MR
deploy_review: 
  stage: deploy_review
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://third-jam-$CI_COMMIT_REF_SLUG.surge.sh
    on_stop: remove_review # on stop trigger remove_review stage
  only: 
    - merge_request    
  script:
    - echo "deploy in third-jam-$CI_COMMIT_REF_SLUG.surge.sh"
    - npm install
    - npm install -g surge
    - surge --project ./public --domain third-jam-$CI_COMMIT_REF_SLUG.surge.sh # https://surge.sh/ - Static web publishing
  except:
   - schedules   

remove_review: 
  stage: deploy_review
  only: 
    - merge_request
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  variables:
    GIT_STRATEGY: none # not clone the specific branch which not longer exist
  when: manual
  script:
    - echo "stopin' review environment"
    - npm install -g surge
    - surge teardown third-jam-$CI_COMMIT_REF_SLUG.surge.sh
  except:
   - schedules

# templates using  anchors. A job with '.' means that is disabled"
.deploy_template: &deploy
  only:
    - main
  environment:
    name: $ENV
    url: http://$DOMAIN  
  script:
    - echo "deploy in $DOMAIN"
    - npm install --global surge
    - surge --project ./public --domain $DOMAIN
  except:
   - schedules  

# Deploying in staging.
deploy_test:
  <<: *deploy
  stage: deploy_staging
  variables:
    DOMAIN: $STAGING_DOMAIN
    ENV: staging

# Deploying in prod
deploy_surge:
  <<: *deploy
  stage: deploy_prod
  variables:
    DOMAIN: http://$PROD_DOMAIN
    ENV: prod
  when: manual # deploy in prod manually
  allow_failure: false # blocks the entire pipelines until the manual deploy is triggered    

smoke_test:
  image: alpine
  stage: post_deploy
  only: 
    - main
  script:
    - apk add --no-cache curl # install curl to image alpine
    - curl -s "http://third-jam.surge.sh" | grep -q "Gatsby"      
    - curl -s "http://third-jam.surge.sh" | grep -q "$CI_COMMIT_SHORT_SHA"
  except:
   - schedules